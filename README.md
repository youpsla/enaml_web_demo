# Enaml-web demo

## Usage

### Clone the repository
```bash
git clone https://gitlab.com/youpsla/enaml_web_demo
```

### cd to the normalize-string directory
```bash
cd enaml_web_demo
```

### Running the script:
```bash
docker compose up
```
THe setup can take up to 5 minutes(DB setup)

### Point your browser to:
[http://localhost:7777](https://github.com/leeoniya/uPlot)

## Various points

Jquery is only used for the [uplot](https://github.com/leeoniya/uPlot) library.

Weekly and Monthly stats are not available.

In the search_slots input in the right of the page, try with "lau" string. This one returns more than one result. Results are clickable even if the mouse pointer is not the right one.