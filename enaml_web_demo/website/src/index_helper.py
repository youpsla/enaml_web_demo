async def update_slots_dropdown(change, providers, slot_dropdown):  
    if change.get('value') != "0":
        provider = [p for p in providers if p.name == change['value']][0]
        slot_dropdown.slots = await provider.slots()
    else:
        slots = None