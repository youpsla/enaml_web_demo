const chart_div = document.querySelector(".chartarea");
var chartNode = $('div[class*=chartdatas]');

$(window).resize(function () {
    sizes = {
        width: $('.chartarea').outerWidth(),
        height: $('.chartarea').outerHeight() - 25
    };
    uplot.setSize(sizes);
});


let opts = {
    id: "chart1",
    width: 600,
    height: 200,
    series: [
        {},
        {
            // initial toggled state (optional)
            show: true,
            spanGaps: false,
            // in-legend display
            label: "RTP",
            value: (self, rawValue) => rawValue.toFixed(2) + ' %',
            // series style
            stroke: "blue",
            fill: "rgba(0,0,255,0.1)",
        }
    ],
}


let data = [[], []];
let uplot = new uPlot(opts, data, chart_div);

// size = chart_div.getBoundingClientRect();
// console.log({width: size.width,height: size.height});
// uplot.setSize({width: size.width,height: size.height});


$(document).ready(function () {
    observer.observe(chartNode[0], observerOptions);
});

// Mutation observer on lightweights chart.
var observer = new MutationObserver(callback);

function callback(mutationsList, observer) {
    mutationsList.forEach((mutation) => {
        if (mutation.type === 'attributes' && mutation.attributeName === 'datas') {
            chartNode.each(function (i, e) {
                data = ($.parseJSON($(e).attr('datas')));
                //console.log(data);
                uplot.setData(data);
            })
        }
    })
}



var observerOptions = {
    childlist: false,
    attributes: true,
    subtree: false
}


