from atom.api import (
    Str,
    Bool,
    Instance,
    Property,
)

from atomdb.sql import SQLModel, SQLModelManager, Relation, find_sql_models
from aiopg.sa import create_engine
import sqlalchemy as sa

import datetime
from decimal import Decimal

import sys
import os

from commons.logger import logger


casinos = [{'name': 'bitcasino', 'url': 'bitcasino.io'}, {'name': 'sportsbet', 'url': 'sportsbet.io'}]

models = []

class Casino(SQLModel):
    name = Str()
    screen_name = Str()
    url = Str()
    dede = Relation(lambda: bitcasino)
    # dudu = Instance(Provider).tag(nullable=False)

for casino in casinos:
    models.append(type(casino['name'], (SQLModel,), {'url': casino['url'], 'Meta' : 'unique_together' = ('name', 'screen_name') }))

for model in find_sql_models():
    print('dede')
    print(model.__name__)

dada = Casino()
print(dada.dede)
