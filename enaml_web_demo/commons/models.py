# coding: utf8
#!/usr/bin/env conda run -n livertp python
import sys
import os
import logging
import datetime
from decimal import Decimal

from atom.api import (
    Str,
    Bool,
    Dict,
    Instance,
)
import sqlalchemy as sa
from atomdb.sql import SQLModel, SQLModelManager, Relation, find_sql_models
from aiopg.sa import create_engine
from sqlalchemy.schema import DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.dialects.postgresql import JSONB

from enaml_web_demo.commons.logger import logger

path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.append(path)

DB_PARAMS = dict(
    host=os.environ["DB_HOST"],
    port=os.environ["DB_PORT"],
    user=os.environ["DB_USER"],
    password=os.environ["DB_USER_PASSWORD"],
    database=os.environ["DB"],
)

logging.basicConfig(level=logging.INFO)

# Update drop_table for postges
@compiles(DropTable, "postgresql")
def _compile_drop_table(element, compiler, **kwargs):
    return compiler.visit_drop_table(element) + " CASCADE"


# class Casino(SQLModel):
#     name = Str().tag(nullable=False, unique=True)
#     screen_name = Str().tag(nullable=False)
#     url = Str().tag(nullable=False)
#     url_rtp = Str().tag(nullable=False)
#     xpaths = Dict().tag(type=JSONB, nullable=False)
#     active = Bool().tag(default=True)
#     providers = Relation(lambda: Provider)
#     slots = Relation(lambda: Slot)

#     class Meta:
#         db_table = "casino"

#     @classmethod
#     async def get_by_name(cls, name):
#         async with create_engine(**DB_PARAMS, echo=True) as engine:
#             mgr = SQLModelManager.instance()
#             mgr.database = engine
#             return await cls.objects.get(name=name)
    
#     @classmethod
#     async def get_all(cls):
#         async with create_engine(**DB_PARAMS, echo=True) as engine:
#             mgr = SQLModelManager.instance()
#             mgr.database = engine
#             return await cls.objects.all()

#     async def create_new_providers(self, scrapped_providers):
#         async with create_engine(**DB_PARAMS) as engine:
#             mgr = SQLModelManager.instance()
#             mgr.database = engine
#             db_providers = await Provider.objects.filter(casino=self)
#             new_providers = {
#                 k: scrapped_providers[k]
#                 for k in set(scrapped_providers)
#                 - set({provider.name: None for provider in db_providers})
#             }
#             if len(new_providers) > 0:
#                 for p, q in new_providers.items():
#                     provider, created = await Provider.objects.get_or_create(
#                         name=p, screen_name=q, casino=self, active=True
#                     )
#                     logger.info(
#                         f"Provider created: {self.screen_name} - {provider.screen_name}"
#                     )
#                     assert created

#     async def create_new_slots(self, scrapped_slots):
#         async with create_engine(**DB_PARAMS) as engine:
#             mgr = SQLModelManager.instance()
#             mgr.database = engine
#             db_slots = await Slot.objects.select_related("provider").filter(casino=self)
#             new_slots = {
#                 k: scrapped_slots[k]
#                 for k in set(scrapped_slots)
#                 - set(
#                     {
#                         slot.name + "_" + slot.provider.name + "_" + self.name: None
#                         for slot in db_slots
#                     }
#                 )
#             }
#             if len(new_slots) > 0:
#                 for p, q in new_slots.items():
#                     provider = await Provider.objects.get(name=q[2])
#                     if provider:
#                         try:
#                             slot, created = await Slot.objects.get_or_create(
#                                 name=q[0],
#                                 screen_name=q[1],
#                                 provider=provider,
#                                 casino=self,
#                             )
#                             assert created
#                         except Exception as e:
#                             logger.info(
#                                 f"{e} - Cannot create slot with name {q[0]} - {provider.name}"
#                             )




class Provider(SQLModel):
    name = Str().tag(nullable=False)
    screen_name = Str().tag(nullable=False)
    # casino = Instance(Casino).tag(nullable=False)
    active = Bool().tag(default=True)
    slots = Relation(lambda: Slot)

    class Meta:
        db_table = "provider"
        # unique_together = ("name", "casino")

    async def slots(self):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine
            slots = await Slot.objects.filter(provider__name=self.name).order_by(
                "screen_name"
            )
            print("tutu")
        return slots

    async def get_active(self):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine
            providers = await self.objects.order_by("screen_name").filter(
                active__is=True
            )
        return providers


class Slot(SQLModel):
    name = Str().tag(nullable=False)
    screen_name = Str().tag(nullable=False)
    # casino = Instance(Casino).tag(nullable=False)
    provider = Instance(Provider).tag(nullable=False)
    active = Bool().tag(default=True)
    dailys = Relation(lambda: Daily)

    class Meta:
        db_table = "slot"
        # unique_together = ("name", "provider", "casino")
        unique_together = ("name", "provider")

    async def get_stats(self, id, table):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine
            q = "SELECT rank, nb, last_dt FROM stats%(table)s WHERE slot = '%(id)s'" % {
                "table": table,
                "id": id,
            }

            async with engine.acquire() as conn:
                r = await conn.execute(q)
                row = await r.fetchone()
        if row:
            last_dt = row[2].strftime("%d %B %H:%M")
            return row[0], row[1] - 1, last_dt
        else:
            return ("NA", "NA", "NA")

    @classmethod
    async def get_details(cls, id):
        async with create_engine(**DB_PARAMS, echo=True) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine
            slot = await cls.objects.select_related("provider").get(_id=id)
        return slot

    async def get_frequency(self, id, hours=48):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine
            timeoffset = datetime.datetime.utcnow() - datetime.timedelta(hours=hours)
            # result = await Daily.objects.filter(slot=f'{id}').filter(dt__gt=timeoffset.strftime("%Y-%m-%d %H:%M:%S")).values('rtp', distinct=True)
            q = (
                sa.select(
                    [
                        sa.func.count(
                            Daily.objects.table.columns.get("rtp").distinct()
                        ).label("rtp"),
                        sa.func.max(Daily.objects.table.columns.get("dt")).label("dt"),
                    ]
                )
                .where(Daily.objects.table.columns.get("dt") >= timeoffset)
                .where(Daily.objects.table.columns.get("slot") == f"{id}")
            )
            row = await Daily.objects.fetchone(q)
            if row.rtp and row.dt:
                return (row.rtp, row.dt.strftime("%d %B %H:%M"))
            else:
                return (0, 0)

    async def last_daily(self, id):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine
            q = (
                # "SELECT * FROM Daily WHERE slot = '%(id)s' and dt IN (SELECT max(dt) FROM Daily)"
                "SELECT * FROM Daily WHERE slot = '%(id)s' order by dt desc limit 1"
                % {"id": id}
            )
            row = await Daily.objects.fetchone(q)
            return row.rtp


class Daily(SQLModel):
    dt = Instance(datetime.datetime).tag(type=sa.DateTime(), index=True)
    rtp = Instance(Decimal).tag(type=sa.Numeric(precision=6, scale=2))
    slot = Instance(Slot).tag(nullable=False)

    class Meta:
        db_table = "daily"
        composite_indexes = [(None, "slot", "dt")]

    async def most_played(self, table="statsdaily", hours=48):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine

            timeoffset = datetime.datetime.utcnow() - datetime.timedelta(hours=hours)
            q = (
                "SELECT slot, nb, rank, last_dt, screen_name from %(table)s where last_dt >= '%(d)s' order by rank limit 100"
                % {"table": table, "d": timeoffset.strftime("%Y-%m-%d %H:%M:%S")}
            )

            data = []
            for row in await Daily.objects.fetchall(q):
                data.append(
                    {
                        "id": row.slot,
                        "count": row.nb - 1,
                        "screen_name": row.screen_name,
                        "rank": row.rank,
                    }
                )
        return data

    async def largest_moves(self, hours=24, direction=">", limit=20):
        async with create_engine(**DB_PARAMS) as engine:
            mgr = SQLModelManager.instance()
            mgr.database = engine

            if direction == ">":
                order_by = "desc"
            else:
                order_by = "asc"

            q = """with t as
					(SELECT
							distinct on (slot)
							slot,
                            first_value(rtp) OVER w firstv,
                            last_value(rtp) OVER w lastv,
                            first_value(dt) OVER w firstdt,
                            last_value(dt) OVER w lastdt
                        FROM Daily where dt >= now() - interval '%(hours)s hour'
                        WINDOW w AS ( PARTITION by  "slot" order by dt RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                        )
                    select  slot, slot.screen_name, firstv, lastv, firstdt::timestamp(0), lastdt::timestamp(0),  lastv - firstv as delta, slot._id from t, slot
                    WHERE (lastv - firstv) %(direction)s 0 AND t.slot = slot._id order by delta %(order_by)s limit %(limit)s
            """ % {
                "hours": hours,
                "direction": direction,
                "order_by": order_by,
                "limit": limit,
            }

            datas = []
            for row in await Daily.objects.fetchall(q):
                datas.append(
                    {
                        "id": row[0],
                        "delta": row[6],
                        "screen_name": row[1],
                        "start_date": row[4],
                        "end_date": row[5],
                        "start_rtp": row[2],
                        "end_rtp": row[3],
                    }
                )
            return datas


class Weekly(SQLModel):
    dt = Instance(datetime.datetime).tag(type=sa.DateTime(), index=True)
    rtp = Instance(Decimal).tag(type=sa.Numeric(precision=6, scale=2))
    slot = Instance(Slot).tag(nullable=False, index=True)

    class Meta:
        db_table = "weekly"


class Monthly(SQLModel):
    dt = Instance(datetime.datetime).tag(type=sa.DateTime(), index=True)
    rtp = Instance(Decimal).tag(type=sa.Numeric(precision=6, scale=2))
    slot = Instance(Slot).tag(nullable=False, index=True)

    class Meta:
        db_table = "monthly"


async def create_stats_view(name):
    async with create_engine(**DB_PARAMS) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine

        durations = {"daily": "24 hours", "weekly": "168 hours", "monthly": "720 hours"}

        q = f"""
                DROP MATERIALIZED VIEW IF EXISTS stats{name} ;
                CREATE MATERIALIZED VIEW stats{name} AS
                select
                slot,
                nb,
                rank,
                last_dt,
                screen_name
                from(
                SELECT
                    slot,
                    count(distinct rtp) as nb,
                    RANK() OVER (
                    order by
                        count(distinct rtp) desc
                    ) as rank,
                    max(dt) as last_dt
                FROM
                    {name}
                where
                    dt > now() - interval '{durations[name]}'
                group by
                    (slot)
                ) as sub, slot where sub.slot = slot._id;
            """
        async with engine.acquire() as conn:
            await conn.execute(q)


async def write_rtps(data):
    async with create_engine(**DB_PARAMS) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine

        provider = data["provider"]
        sort = data["sort"]
        dt_now = datetime.datetime.now()

        for i in data["records"]:
            screen_name = i[0]
            name = i[1]
            rtp = i[2]

            try:
                slot, created = await Slot.objects.get_or_create(
                    name=name, screen_name=screen_name, provider=provider
                )

                if sort == "rtpLivePastDay":
                    record = Daily(dt=dt_now, slot=slot, rtp=Decimal(rtp))
                elif sort == "rtpLivePastSevenDays":
                    record = Weekly(dt=dt_now, slot=slot, rtp=Decimal(rtp))
                elif sort == "rtpLivePastThirtyDays":
                    record = Monthly(dt=dt_now, slot=slot, rtp=Decimal(rtp))
                else:
                    pass
                await record.save()
            except:
                print(
                    f"Cannot create {screen_name} from provider {provider}. Duplicate slot name"
                )


async def write_rtps_2(casino, data):
    async with create_engine(**DB_PARAMS) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine

        sort = data["sort"]
        dt_now = datetime.datetime.now()

        for i in data["records"]:
            slot_name = i["slot_name"]
            provider_name = i["provider_name"]
            rtp = i["rtp"]

            if provider_name and slot_name and rtp:
                try:
                    provider = await Provider.objects.get(
                        name=provider_name, casino=casino
                    )
                    slot = await Slot.objects.get(
                        name=slot_name, provider=provider, casino=casino
                    )
                    if sort == "rtpLivePastDay":
                        record = Daily(
                            dt=dt_now,
                            slot=slot,
                            rtp=Decimal(rtp),
                        )
                    elif sort == "rtpLivePastSevenDays":
                        record = Weekly(
                            dt=dt_now,
                            slot=slot,
                            rtp=Decimal(rtp),
                        )
                    elif sort == "rtpLivePastThirtyDays":
                        record = Monthly(
                            dt=dt_now,
                            slot=slot,
                            rtp=Decimal(rtp),
                        )
                    else:
                        pass
                    await record.save()
                except Exception as e:
                    logger.info(e)
                    logger.info(i)


async def reset_tables():
    # import logging

    # logging.basicConfig()
    # logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    async with create_engine(**DB_PARAMS, echo=True) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine
        mgr.create_tables()
        for Model in find_sql_models():
            try:
                await Model.objects.drop_table()
            except Exception as e:
                msg = str(e)
                if not ("Unknown table" in msg or "does not exist" in msg):
                    raise  # Unexpected error
            await Model.objects.create_table()

        views = ["daily", "weekly", "monthly"]
        for view in views:
            await create_stats_view(view)

        # await insert_casinos_in_db()


# async def insert_casinos_in_db():
#     # from const import CASINOS

#     for casino in CASINOS:
#         async with create_engine(**DB_PARAMS, echo=True) as engine:
#             mgr = SQLModelManager.instance()
#             mgr.database = engine
#             await Casino.objects.create(
#                 name=casino["name"],
#                 screen_name=casino["screen_name"],
#                 url=casino["url"],
#                 url_rtp=casino["url_rtp"],
#                 xpaths=casino["xpaths"],
#                 active=True,
#             )


# async def update_casino_xpaths():
#     from const import CASINOS

#     async with create_engine(**DB_PARAMS, echo=True) as engine:
#         mgr = SQLModelManager.instance()
#         mgr.database = engine
#         for casino in CASINOS:
#             await Casino.objects.filter(name=casino["name"]).update(xpaths=casino["xpaths"])


if __name__ == "__main__":
    import argparse
    import asyncio

    parser = argparse.ArgumentParser()
    parser.add_argument("--reset_tables", help="Delete and recreate all tables.")
    parser.add_argument("--update_xpaths", help="Update xpaths for each casino")
    args = parser.parse_args()

    if args.reset_tables:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(reset_tables())

    if args.update_xpaths:
        loop = asyncio.get_event_loop()
        # loop.run_until_complete(update_casino_xpaths())
