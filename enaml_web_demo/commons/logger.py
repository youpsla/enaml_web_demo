import logging

# Define logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s :: %(levelname)s :: %(message)s")

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)

# file_handler = logging.FileHandler('/tmp/rtp.log')
# file_handler.setLevel(logging.DEBUG)
# file_handler.setFormatter(formatter)

logger.addHandler(stream_handler)
# logger.addHandler(file_handler)

