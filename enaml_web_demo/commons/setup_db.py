from aiopg.sa import create_engine
from atomdb.sql import SQLModelManager, find_sql_models
import asyncio
import os
from enaml_web_demo.commons.models import Provider, Slot, Daily, create_stats_view
from faker import Faker
import string
import random
from decimal import Decimal

DB_PARAMS = dict(
    host=os.environ["DB_HOST"],
    port=os.environ["DB_PORT"],
    user=os.environ["DB_USER"],
    password=os.environ["DB_USER_PASSWORD"],
    database=os.environ["DB"],
)


async def check_if_db_exist():
    try:
        async with create_engine(**DB_PARAMS, echo=True) as engine:
            pass
        # mgr = SQLModelManager.instance()
        # mgr.database = engine
        # with engine.connect() as con:
        #     con.execute("select 1")
        #     print("dodo")
    except Exception as e:
        print(e)
        await setup_db()
    return


async def setup_db():

    print("Try to create livertp db")
    import copy
    postgres_DB_PARAMS = copy.deepcopy(DB_PARAMS)
    postgres_DB_PARAMS["database"] = "postgres"
    async with create_engine(**postgres_DB_PARAMS, echo=True) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine
        async with engine.acquire() as con:
            try:
                r = await con.execute("CREATE DATABASE livertp")
                print("Db created")
            except Exception as e:
                print(e)
            
    
    # Create tables
    async with create_engine(**DB_PARAMS, echo=True) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine
        mgr.create_tables()
        for Model in find_sql_models():
            try:
                await Model.objects.drop_table()
            except Exception as e:
                msg = str(e)
                if not ("Unknown table" in msg or "does not exist" in msg):
                    raise  # Unexpected error
            await Model.objects.create_table()



async def create_providers():
    Faker.seed(0)
    fake = Faker()
    async with create_engine(**DB_PARAMS) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine

        for _ in range(2):
            comp = fake.company()
            p = Provider(
                name=comp,
                screen_name=comp.upper(),
                active=True
            )
            await p.save()


async def create_slots():
    Faker.seed(0)
    fake = Faker()
    async with create_engine(**DB_PARAMS) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine

        providers = await Provider.objects.all()

        for p in providers:
            for i in range(80, 100):
                # tmp_name = string.ascii_lowercase
                # slot_name = ''.join(random.choice(tmp_name) for i in range(random.randint(10, 50)))
                # print(slot_name)
                slot_name = fake.name()
                s = Slot(
                    name=slot_name,
                    screen_name=slot_name.upper(),
                    active=True,
                    provider = p
                )
                await s.save()



async def create_daily():
    Faker.seed(0)
    fake = Faker()
    async with create_engine(**DB_PARAMS) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine

        slots = await Slot.objects.all()

        for s in slots:
            ts = fake.time_series(start_date="-15d", precision=random.randint(1000, 2000), distrib=lambda a: random.uniform(20, 400))
            for t in ts:

                d = Daily(
                    rtp=Decimal(t[1]),
                    dt=t[0],
                    slot = s
                )
                await d.save()


def full_setup():

    print('\nFirst, check if db exist')
    asyncio.run(check_if_db_exist())

    print('\nSetup Db structure')
    asyncio.run(setup_db())

    print('\nCreate providers')
    asyncio.run(create_providers())

    print('\nCreate slots')
    asyncio.run(create_slots())

    print('\nCreate daily. Will spend around 5 minutes')
    asyncio.run(create_daily())

    print("\nCreate daily view")
    asyncio.run(create_stats_view("daily"))



if __name__ == "__main__":

    full_setup()



